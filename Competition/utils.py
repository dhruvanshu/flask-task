from app import ma
from .models import Competition
from User.utils import UserSerializer
from User.models import User

class CompetitionSerializer(ma.Schema):


    class Meta:
        model = Competition   
        fields = ('id', 'title', 'social_issue', 'user_competition')    

    user_competition = ma.Nested(UserSerializer(only=('id','name',)))

