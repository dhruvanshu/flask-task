
from flask import Flask, Blueprint
from flask_migrate import Migrate
from database import db
from flask_marshmallow import Marshmallow

from User.models import *
from Competition.models import *
from Entry.models import *

flask_app = Flask(__name__)
flask_app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:admin@localhost:5432/task"

db.init_app(flask_app)
migrate = Migrate(flask_app, db)
ma = Marshmallow(flask_app)


# blueprint register
from User.api import user_blueprint
from Competition.api import competition_blueprint
from Entry.api import entry_blueprint
flask_app.register_blueprint(user_blueprint)
flask_app.register_blueprint(competition_blueprint)
flask_app.register_blueprint(entry_blueprint)

