import uuid
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import backref

from database import db
from User.models import CommonModel, User
from Competition.models import Competition


class Entry(CommonModel):
    __tablename__ = 'entries'

    id = db.Column(UUID(as_uuid=True),
                   primary_key=True, default=uuid.uuid4)
    user_id = db.Column(UUID, db.ForeignKey('users.id'), nullable=True)
    
    name = db.Column(db.String())
    state = db.Column(db.String())
    how_did_you_hear = db.Column(db.String())
    competition_id = db.Column(UUID, db.ForeignKey(
        'competitions.id'), nullable=True)
    is_entrant_part_of_institution = db.Column(db.Boolean, default=False)
    i_am_part_of = db.Column(db.String())
    


    def __repr__(self):
        return f"<Entry {self.name}>"
