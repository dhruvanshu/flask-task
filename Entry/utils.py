from app import ma 
from User.utils import UserSerializer
from Competition.utils import CompetitionSerializer
from marshmallow import Schema, fields, pprint
from .models import Entry


class EntrySerializer(ma.Schema):
    class Meta:
        model = Entry
        fields = ('id','user_id','name','state','how_did_you_her','competition_id','is_entrant_part_of_institution','i_am_part_of')



