from flask import Blueprint, request, jsonify
from flask_restful import Resource, Api
from database import db

from .models import User
from .utils import UserSerializer


user_blueprint = Blueprint('user', __name__)

api = Api(user_blueprint)


class UserApi(Resource):

    serializer_class = UserSerializer()

    def post(self, *args, **kwargs):
        name = request.json.get('name', None)
        email = request.json.get('email', None)
        phone_number = request.json.get('phone_number', None)
        gender = request.json.get('gender', None)

        user = User(name=name, email=email,
                    phone_number=phone_number, gender=gender)

        db.session.add(user)
        db.session.commit()

        return jsonify({'message': 'Added Successfully'})

    def get(self, *args, **kwargs):
        id = kwargs.pop('id', None)
        if id:
            user = User.query.filter_by(id=id).first()
            data = self.serializer_class.dump(user)
            return jsonify(data)
        else:
            user = User.query.all()
            data = self.serializer_class.dump(user, many=True)
            return jsonify(data)
    
    def patch(self, *args, **kwargs):
        id = kwargs.pop('id', None)
        try:
            user = User.query.filter_by(id=id).first()

            if user:
                user.name = request.json.get('name', user.name)
                user.email = request.json.get('email', user.email)
                user.phone_number = request.json.get('phone_number', user.phone_number)
                user.gender = request.json.get('gender', user.gender)
                db.session.commit()
                return jsonify({'message': 'Update Successfully'})
        except:
            return jsonify({'message': 'User not found'})
    
       

    


api.add_resource(UserApi, '/user', '/user/<id>')
